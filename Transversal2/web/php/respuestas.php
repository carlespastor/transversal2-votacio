<?php
//Conexion a la base de datos
include 'db.php';

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$idPregunta = $_POST["idpregunta"];

$query = "SELECT DISTINCT (SELECT count(valor) FROM resposta WHERE valor=1 and id_enquesta='$idPregunta') AS si,(SELECT count(valor) FROM resposta WHERE valor=0 and id_enquesta='$idPregunta') AS no FROM resposta;";

$resultado = mysqli_query($conn, $query);



if(!$resultado){
    die("Error");
}else{
    while($data = mysqli_fetch_object($resultado)) {
        $respuestas[] = $data;
    }
}

$cod = json_encode($respuestas);
echo $cod;

mysqli_free_result($resultado);
mysqli_close($conn);
?>
