$(document).ready(function(){
    getCookie("rol");
})

//Funcion JS que recupera la cookie "rol" y comprueba que estes logueado.
function getCookie(cookiename){
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    var username = getCookie(cookiename);
    if (username == "user") {
        console.log("Permiso concedido con privilegios de usuario.");
        showUserDesktop();
    }

    else if (username == "admin") {
        console.log("Permiso concedido con privilegios de administrador.");
        showAdminDesktop();
    }

    else {
        console.log("No hay usuarios loggueados");
        tabsAction();
    }

}
