$(document).ready(function(){
    var x = $("#submit");
    x.click(accede);
})

function accede(){
    parametros={
        usuario : $("#user").val(),
        pass : $("#pass").val()
    };
    $.ajax({
        type:'POST',
        data: parametros,
        url: 'php/login.php',
        dataType : 'JSON',
        success: function (datos){
            if(datos.admin==1) sesionAdmin(datos.username, datos.id);
            else if(datos.admin==0) sesionUser(datos.username, datos.id);
            else $(".error").html("*Error: Datos incorrectos");
            console.log(datos.admin);
        },
        error: function (datos){
            console.log("Error en comprovacion de usuarios");
        }
    })
}

function sesionUser(uname, iduser){
    document.cookie = "rol=user";
    document.cookie = "uname=" +uname;
    document.cookie = "idUser=" +iduser;
    showUserDesktop();
}

function sesionAdmin(uname, iduser) {

    document.cookie = "rol=admin";
    document.cookie = "uname=" +uname;
    document.cookie = "idUser=" +iduser;
    showAdminDesktop();

}

function showUserDesktop() {

    $.ajax({
        type:'POST',
        url: 'user',
        dataType : 'html',
        success: function (datos){
            $('body').html(datos);
            $('#myTab').css('display', "none");
            dameEncuestas();
        },
        error: function (){
            console.log("Ha dado error");
        }
    })
}

function showAdminDesktop() {

    $.ajax({
        type:'POST',
        url: 'admin',
        dataType : 'html',
        success: function (datos){
            //console.log(datos);
            $('body').html(datos);
            $('#myTab').css('display', "none");
        },
        error: function (){
            console.log("Ha dado error");
        }
    })
}