function tabsAction(){
        muestraDestacados();
}

function muestraDestacados(){
    var x = $(".login");
    var y = $(".encuestastop");
    x.click(muestraLogin);
    y.click(muestraDestacados);
    $.ajax({
        type:'POST',
        url: 'toptoll',
        dataType : 'html',
        success: function (datos){
            $('.contentajax').html(datos);
        },
        error: function (){
            console.log("Ha dado error");
        }
    })
}


//La funcion muestraLoguin es llamada desde muestraDestacados
function muestraLogin(){
    $.ajax({
        type:'POST',
        url: 'login',
        dataType : 'html',
        beforeSend: function(){
            $(".login").addClass("active");
            $(".encuestastop").removeClass("active");
        },
        success: function (datos){
            $('.contentajax').html(datos);
        },
        error: function (datos){
            console.log("Ha dado error");
        }
    })
}