$(document).ready(function(){
        $("form").submit(function(event){
            var any_inici = $("#appbundle_enquesta_dataInici_year").val(),
                mes_inici = $("#appbundle_enquesta_dataInici_month").val()-1,
                dia_inici = parseInt($("#appbundle_enquesta_dataInici_day").val()),
                any_final = $("#appbundle_enquesta_dataFinal_year").val(),
                mes_final = $("#appbundle_enquesta_dataFinal_month").val()-1,
                dia_final = parseInt($("#appbundle_enquesta_dataFinal_day").val());
            var data_inici = new Date(any_inici, mes_inici, dia_inici,0,0,0,0),
                data_final = new Date(any_final, mes_final, dia_final),
                data_actual = new Date();

            data_actual.setHours(2,0,0,0);
            data_inici.setHours(2,0,0,0);
            data_final.setHours(2,0,0,0);

            if(data_inici >= data_final || data_inici <= data_actual){
                    event.preventDefault();
                    alert("Error amb les dates indicades.");
            }
        });
})
