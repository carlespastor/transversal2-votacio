$(document).ready(function(){
    dameDestacadas();
})

function dameDestacadas(){
    $.ajax({
        type:'POST',
        url: 'php/destacadas.php',
        dataType : 'JSON',
        success: function (datos){
            //console.log(datos[0].pregunta);
            var limit = (Object.keys(datos).length);
            if (limit>=9){
                for (var i=0;i<9;i++){
                    $(".pregunta" +(i+1)).html(datos[i].pregunta);
                    $("#name"+(i+1)).html(datos[i].autor);
                }
            }
            else{
                for (var i=0;i<limit;i++){
                    $(".pregunta" +(i+1)).html(datos[i].pregunta);
                    $("#name"+(i+1)).html(datos[i].autor);
                }
                for (var i=limit;i<9;i++){
                    $(".q" +(i+1)).addClass("dnone");
                }
            }

            $(".clickresultados").click(function () {

                var clase = $(this).parent().attr("class");
                var limpio = clase.substring(7,8);
                var dest = datos[limpio-1].id;
                console.log(dest);
                muestraresultados(dest);
                console.log("q" + limpio + " .clickresultados");
                $(".q" + limpio + " .clickresultados").html('<div style="width:50%"><canvas id="myChart' + dest + '" width="10" height="10"></canvas></div>');
            });
            //console.log(limit);
            //if(datos[0].autor=null) console.log("Anonimo");
        },
        error: function (datos){
            console.log("Error en destacadas");
        }
    })
}

function muestraresultados(idtoll) {
    parametros={
        idpregunta : idtoll
    };
    $.ajax({
        type:'POST',
        data: parametros,
        url: 'php/respuestas.php',
        dataType : 'JSON',
        success: function (data){

            console.log(data[0].si);
            console.log(data[0].no);

            var ctx = document.getElementById("myChart" + idtoll);
            var myChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: ["Si", "No"],
                    datasets: [{
                        label: '# of Votes',
                        data: [data[0].si, data[0].no],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)'
                        ],
                        borderWidth: 1
                    }]
                },

            });

        },
        error: function (data){
            console.log("Error en comprovacion de respuestas");
        }
    })
}

// function muestragraficos(idtoll, idbuttonclicked){
//
//     parametros={
//         idpregunta : idtoll
//     };
//     $.ajax({
//         type:'POST',
//         data: parametros,
//         url: 'php/respuestas.php',
//         dataType : 'JSON',
//         success: function (data){
//
//             console.log(data[0].si);
//             console.log(data[0].no);
//
//             var ctx = document.getElementById("myChart" + idtoll);
//             var myChart = new Chart(ctx, {
//                 type: 'doughnut',
//                 data: {
//                     labels: ["Si", "No"],
//                     datasets: [{
//                         label: '# of Votes',
//                         data: [data[0].si, data[0].no],
//                         backgroundColor: [
//                             'rgba(255, 99, 132, 0.2)',
//                             'rgba(54, 162, 235, 0.2)'
//                         ],
//                         borderColor: [
//                             'rgba(255,99,132,1)',
//                             'rgba(54, 162, 235, 1)'
//                         ],
//                         borderWidth: 1
//                     }]
//                 },
//
//             });
//
//         },
//         error: function (data){
//             console.log("Error en comprovacion de respuestas");
//         }
//     })
//     muestrarespuestauser(idtoll, idbuttonclicked);
// }
