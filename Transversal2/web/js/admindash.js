$(document).ready(function(){
    dameNombre();
})

function dameNombre(){

    var username = getCookie("uname");
    $("#nameadmin").html(username);
    $("#nameadmin").css("text-transform", "capitalize");
    $("#logo").click(function () {
        location.reload();
    });
    $("#logout").click(logoutAdmin);
    $(".fa-plus").click(function () {
        location.replace("editar/new");
    });
    $(".fa-edit").click(function () {
        location.replace("editar");
    });
    $(".fa-cogs").click(function () {
        location.replace("settings");
    });
}


function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function logoutAdmin() {
    document.cookie = 'rol=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    document.cookie = 'uname=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    document.cookie = 'comp=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    document.cookie = 'idUser=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    //location.replace("/");
}



