<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Enquesta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Enquestum controller.
 *
 * @Route("editar")
 */
class EnquestaController extends Controller
{
    /**
     * Lists all enquestum entities.
     *
     * @Route("/", name="enquesta_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $enquestas = $em->getRepository('AppBundle:Enquesta')->findAll();
        $respostes = $em->getRepository('AppBundle:Resposta')->findAll();

        return $this->render('enquesta/index.html.twig', array(
            'enquestas' => $enquestas,
            'respostes' => $respostes,
        ));
    }

    /**
     * Creates a new enquestum entity.
     *
     * @Route("/new", name="enquesta_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $enquestum = new Enquesta();
        $form = $this->createForm('AppBundle\Form\EnquestaType', $enquestum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($enquestum);
            $em->flush();

            return $this->redirectToRoute('enquesta_index');
        }

        return $this->render('enquesta/new.html.twig', array(
            'enquestum' => $enquestum,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a enquestum entity.
     *
     * @Route("/{id}", name="enquesta_show")
     * @Method("GET")
     */
    public function showAction(Enquesta $enquestum, $id)
    {
        $deleteForm = $this->createDeleteForm($enquestum);
        $repository = $this->getDoctrine()->getEntityManager()->getRepository('AppBundle:Resposta');
        $resp = $repository->findByIdEnquesta($id);

        return $this->render('enquesta/show.html.twig', array(
            'enquestum' => $enquestum,
            'respostes' => $resp,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing enquestum entity.
     *
     * @Route("/{id}/edit", name="enquesta_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Enquesta $enquestum)
    {
        $deleteForm = $this->createDeleteForm($enquestum);
        $editForm = $this->createForm('AppBundle\Form\EnquestaType', $enquestum);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('enquesta_edit', array('id' => $enquestum->getId()));
        }

        return $this->render('enquesta/edit.html.twig', array(
            'enquestum' => $enquestum,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a enquestum entity.
     *
     * @Route("/{id}", name="enquesta_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Enquesta $enquestum, $id)
    {
        $form = $this->createDeleteForm($enquestum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //Borra tant la enquesta com les seves respostes
            $em = $this->getDoctrine()->getManager();
            $repository = $this->getDoctrine()->getEntityManager()->getRepository('AppBundle:Resposta');
            $resp = $repository->findByIdEnquesta($id);
            foreach ($resp as $item){
                $em->remove($item);
            }
            $em->remove($enquestum);
            $em->flush();
        }

        return $this->redirectToRoute('enquesta_index');
    }

    /**
     * Creates a form to delete a enquestum entity.
     *
     * @param Enquesta $enquestum The enquestum entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Enquesta $enquestum)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('enquesta_delete', array('id' => $enquestum->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
