<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Usuari;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Usuari controller.
 *
 * @Route("usuari")
 */
class UsuariController extends Controller
{
    /**
     * Lists all usuari entities.
     *
     * @Route("/", name="usuari_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $usuaris = $em->getRepository('AppBundle:Usuari')->findAll();

        return $this->render('usuari/index.html.twig', array(
            'usuaris' => $usuaris,
        ));
    }

    /**
     * Creates a new usuari entity.
     *
     * @Route("/new", name="usuari_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $usuari = new Usuari();
        $form = $this->createForm('AppBundle\Form\UsuariType', $usuari);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($usuari);
            $em->flush();

            return $this->redirectToRoute('usuari_show', array('id' => $usuari->getId()));
        }

        return $this->render('usuari/new.html.twig', array(
            'usuari' => $usuari,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a usuari entity.
     *
     * @Route("/{id}", name="usuari_show")
     * @Method("GET")
     */
    public function showAction(Usuari $usuari)
    {
        $deleteForm = $this->createDeleteForm($usuari);

        return $this->render('usuari/show.html.twig', array(
            'usuari' => $usuari,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing usuari entity.
     *
     * @Route("/{id}/edit", name="usuari_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Usuari $usuari)
    {
        $deleteForm = $this->createDeleteForm($usuari);
        $editForm = $this->createForm('AppBundle\Form\UsuariType', $usuari);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('usuari_edit', array('id' => $usuari->getId()));
        }

        return $this->render('usuari/edit.html.twig', array(
            'usuari' => $usuari,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a usuari entity.
     *
     * @Route("/{id}", name="usuari_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Usuari $usuari)
    {
        $form = $this->createDeleteForm($usuari);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($usuari);
            $em->flush();
        }

        return $this->redirectToRoute('usuari_index');
    }

    /**
     * Creates a form to delete a usuari entity.
     *
     * @param Usuari $usuari The usuari entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Usuari $usuari)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('usuari_delete', array('id' => $usuari->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
