<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AppController extends Controller
{


    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        //redirecciones
        //return $this->redirect($this->generateUrl("helloworld"));
        //return $this->redirect($request->getBaseUrl()."/helloworld?hola=true");
//        if(session_id() != '') {
//            session_destroy();
//        }
        // replace this example code with whatever you need
        return $this->render('@App/index/index.html.twig', array(
            'texto' => "Index",
        ));
    }

//    /**
//     * @Route("/vuelveindex", name="vuelveindex")
//     */
//    public function vuelveLoginAction(Request $request)
//    {
//
//        return $this->redirect($this->generateUrl("homepage"));
//    }

    /**
     * @Route("/toptoll", name="toptoll")
     */
    public function toptollTemplateAction(Request $request)
    {

        // replace this example code with whatever you need
        return $this->render('@App/templates/toptoll.html.twig', array(
            'texto' => "Index",
        ));
    }

    /**
     * @Route("/login", name="login")
     */
    public function loginTemplateAction(Request $request)
    {

        // replace this example code with whatever you need
        return $this->render('@App/templates/login.html.twig', array(
            'texto' => "Index",
        ));
    }

    /**
 * @Route("/user", name="user")
 */
    public function userAction(Request $request)
    {
        if (isset($_SESSION['rol']) && isset($_SESSION['uname'])){
            $rol = $_SESSION['rol'];
            $uname=$_SESSION['uname'];

            // replace this example code with whatever you need
            return $this->render('@App/user/userDesktop.html.twig', array(
                'texto' => "Responde",
                'rol' => $rol,
                'uname' => $uname
            ));
        }

        else{
//            if(session_id() != '') {
//                session_destroy();
//            }
            // replace this example code with whatever you need
            return $this->render('@App/user/userDesktop.html.twig', array(
                'texto' => "Responde",
                'rol' => "norol",
                'uname' => "nouname"
            ));
        }

    }

    /**
     * @Route("/admin", name="admin")
     */
    public function adminAction(Request $request)
    {

        // replace this example code with whatever you need
        return $this->render('@App/admin/adminDesktop.html.twig', array(
            'texto' => "Admin",
        ));
    }

    /**
     * @Route("/settings", name="adminsettings")
     */
    public function settingsAction(Request $request)
    {

        // replace this example code with whatever you need
        return $this->render('@App/templates/settings.html.twig', array(
            'texto' => "Admin",
        ));
    }



}
