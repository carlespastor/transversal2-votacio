<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Resposta
 *
 * @ORM\Table(name="resposta")
 * @ORM\Entity
 */
class Resposta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="valor", type="integer", nullable=false)
     */
    private $valor;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data", type="datetime", nullable=false)
     */
    private $data = 'CURRENT_TIMESTAMP';

    /**
     * @var integer
     *
     * @ORM\Column(name="id_usuari", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idUsuari;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_enquesta", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idEnquesta;

    function getValor() {
        return $this->valor;
    }

    function getData() {
        return $this->data;
    }

    function getIdUsuari() {
        return $this->idUsuari;
    }

    function getIdEnquesta() {
        return $this->idEnquesta;
    }

    function setValor($valor) {
        $this->valor = $valor;
    }

    function setData(\DateTime $data) {
        $this->data = $data;
    }

    function setIdUsuari($idUsuari) {
        $this->idUsuari = $idUsuari;
    }

    function setIdEnquesta($idEnquesta) {
        $this->idEnquesta = $idEnquesta;
    }
}

