<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Enquesta
 *
 * @ORM\Table(name="enquesta")
 * @ORM\Entity
 */
class Enquesta
{
    /**
     * @var string
     *
     * @ORM\Column(name="pregunta", type="string", length=250, nullable=false)
     */
    private $pregunta;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_inici", type="date", nullable=false)
     */
    private $dataInici;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_final", type="date", nullable=false)
     */
    private $dataFinal;

    /**
     * @var boolean
     *
     * @ORM\Column(name="destacada", type="boolean", nullable=false)
     */
    private $destacada = false;

    /**
     * @var string
     *
     * @ORM\Column(name="autor", type="string", length=15, nullable=false)
     */
    private $autor = 'Anonimo';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    function getPregunta() {
        return $this->pregunta;
    }

    function getDataInici() {
        return $this->dataInici;
    }

    function getDataFinal() {
        return $this->dataFinal;
    }

    function getDestacada() {
        return $this->destacada;
    }

    function getAutor() {
        return $this->autor;
    }

    function getId() {
        return $this->id;
    }

    function setPregunta($pregunta) {
        $this->pregunta = $pregunta;
    }

    function setDataInici(\DateTime $dataInici) {
        $this->dataInici = $dataInici;
    }

    function setDataFinal(\DateTime $dataFinal) {
        $this->dataFinal = $dataFinal;
    }

    function setDestacada($destacada) {
        $this->destacada = $destacada;
    }

    function setAutor($autor) {
        $this->autor = $autor;
    }

    function setId($id) {
        $this->id = $id;
    }

}
