<?php

/* @App/admin/adminDesktop.html.twig */
class __TwigTemplate_da904ac3bbec8877682dc090ee4f591a94f88a2600a889b03049aa5eecb437d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/admin/adminDesktop.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/admin/adminDesktop.html.twig"));

        // line 1
        echo "<nav class=\"navbar navbar-light bg-light\">
    <span id=\"logo\" class=\"btn-answer navbar-brand\">
        <img src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/logo.png"), "html", null, true);
        echo "\" height=\"55\" alt=\"\">
    </span>
    <p class=\"text-right\">Bienvenido de nuevo <span id=\"nameadmin\"></span> !<br><a href=\"";
        // line 5
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\"><span id=\"logout\" class=\"btn-answer\">Logout <i class=\"fa fa-user-times\"></i></span></a></p>
</nav>
<div class=\"container\">
    <div class=\"row admindash\">
        <div class=\"text-center col-sm\">
            <i class=\"fa fa-plus\"></i>
            <p>Añadir Encuesta</p>
        </div>
        <div class=\"text-center col-sm\">
            <i class=\"fa fa-edit\"></i>
            <p>Editar Encuesta</p>
        </div>
        <div class=\"text-center col-sm\">
            <i class=\"fa fa-cogs\"></i>
            <p>Opciones de Administrador</p>
        </div>
    </div>
</div>
<script src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/admindash.js"), "html", null, true);
        echo "\"></script>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@App/admin/adminDesktop.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 23,  34 => 5,  29 => 3,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<nav class=\"navbar navbar-light bg-light\">
    <span id=\"logo\" class=\"btn-answer navbar-brand\">
        <img src=\"{{ asset('img/logo.png') }}\" height=\"55\" alt=\"\">
    </span>
    <p class=\"text-right\">Bienvenido de nuevo <span id=\"nameadmin\"></span> !<br><a href=\"{{ path('homepage') }}\"><span id=\"logout\" class=\"btn-answer\">Logout <i class=\"fa fa-user-times\"></i></span></a></p>
</nav>
<div class=\"container\">
    <div class=\"row admindash\">
        <div class=\"text-center col-sm\">
            <i class=\"fa fa-plus\"></i>
            <p>Añadir Encuesta</p>
        </div>
        <div class=\"text-center col-sm\">
            <i class=\"fa fa-edit\"></i>
            <p>Editar Encuesta</p>
        </div>
        <div class=\"text-center col-sm\">
            <i class=\"fa fa-cogs\"></i>
            <p>Opciones de Administrador</p>
        </div>
    </div>
</div>
<script src=\"{{ asset('js/admindash.js') }}\"></script>", "@App/admin/adminDesktop.html.twig", "/var/www/html/thepoll/src/AppBundle/Resources/views/admin/adminDesktop.html.twig");
    }
}
