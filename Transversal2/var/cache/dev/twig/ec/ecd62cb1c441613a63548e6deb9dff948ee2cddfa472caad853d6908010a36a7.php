<?php

/* enquesta/index.html.twig */
class __TwigTemplate_ffc78f3109bef92aeae6390c7e511964af4410a197727426438643d610a1996c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "enquesta/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "enquesta/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "enquesta/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "\t<nav class=\"navbar navbar-light bg-light\">
    <span id=\"logo\" class=\"btn-answer navbar-brand\">
        <img src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/logo.png"), "html", null, true);
        echo "\" height=\"55\" alt=\"\">
    </span>
        <span class=\"functionsplus\"><a href=\"";
        // line 8
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("enquesta_new");
        echo "\"><i class=\"fa fa-plus\"></i></a></span>
    <p class=\"text-right\">Bienvenido de nuevo <span id=\"nameadmin\"></span> !<br><a id=\"backtodash\" href=\"";
        // line 9
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\" class=\"btn-answer\">Dashboard <i class=\"fa fa-user-circle\"></i></a></p>
\t</nav>
\t<div class=\"container\">    
\t<h1>Llista d'enquestes</h1>
    <table class=\"tabladmin\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Pregunta</th>
                <th>Data inicial</th>
                <th>Data final</th>
                <th>Destacada</th>
                <th>Autor</th>                
                <th>Respostes</th>
                <th>Activa</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_reverse_filter($this->env, ($context["enquestas"] ?? $this->getContext($context, "enquestas"))));
        foreach ($context['_seq'] as $context["_key"] => $context["enquestum"]) {
            // line 29
            echo "            <tr>
                <td>";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["enquestum"], "id", array()), "html", null, true);
            echo "</td>
                <td><a href=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("enquesta_show", array("id" => $this->getAttribute($context["enquestum"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["enquestum"], "pregunta", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 32
            if ($this->getAttribute($context["enquestum"], "dataInici", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["enquestum"], "dataInici", array()), "Y-m-d"), "html", null, true);
            }
            echo "</td>
                <td>";
            // line 33
            if ($this->getAttribute($context["enquestum"], "dataFinal", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["enquestum"], "dataFinal", array()), "Y-m-d"), "html", null, true);
            }
            echo "</td>
                <td>
                    ";
            // line 35
            if (($this->getAttribute($context["enquestum"], "destacada", array()) == 1)) {
                // line 36
                echo "                        Si
                    ";
            } else {
                // line 38
                echo "                        No
                    ";
            }
            // line 40
            echo "                </td>
                <td>";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($context["enquestum"], "autor", array()), "html", null, true);
            echo "</td>                
                <td>
                    <ul>
                        ";
            // line 44
            list($context["si"], $context["no"]) =             array(0, 0);
            // line 45
            echo "                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["respostes"] ?? $this->getContext($context, "respostes")));
            foreach ($context['_seq'] as $context["_key"] => $context["resposta"]) {
                // line 46
                echo "                            ";
                if (($this->getAttribute($context["resposta"], "idEnquesta", array()) == $this->getAttribute($context["enquestum"], "id", array()))) {
                    // line 47
                    echo "                                ";
                    if (($this->getAttribute($context["resposta"], "valor", array()) == 1)) {
                        // line 48
                        echo "                                    ";
                        $context["si"] = (($context["si"] ?? $this->getContext($context, "si")) + 1);
                        // line 49
                        echo "                                ";
                    } else {
                        // line 50
                        echo "                                    ";
                        $context["no"] = (($context["no"] ?? $this->getContext($context, "no")) + 1);
                        // line 51
                        echo "                                ";
                    }
                    echo "  
                            ";
                }
                // line 53
                echo "                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['resposta'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 54
            echo "                        <li> 
                            Si: ";
            // line 55
            echo twig_escape_filter($this->env, ($context["si"] ?? $this->getContext($context, "si")), "html", null, true);
            echo "                            
                        </li>
                        <li>
                            No: ";
            // line 58
            echo twig_escape_filter($this->env, ($context["no"] ?? $this->getContext($context, "no")), "html", null, true);
            echo "
                        </li>
                        <li>
                            Total: ";
            // line 61
            echo twig_escape_filter($this->env, (($context["si"] ?? $this->getContext($context, "si")) + ($context["no"] ?? $this->getContext($context, "no"))), "html", null, true);
            echo "
                        </li>
                    </ul>
                </td>
                <td>
                    ";
            // line 66
            if ((twig_date_converter($this->env, $this->getAttribute($context["enquestum"], "dataFinal", array())) > twig_date_converter($this->env, twig_date_format_filter($this->env, "now", "Y/m/d")))) {
                // line 67
                echo "                        Si
                    ";
            } else {
                // line 69
                echo "                        No
                    ";
            }
            // line 71
            echo "                </td>
                <td>
                    <a href=\"";
            // line 73
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("enquesta_edit", array("id" => $this->getAttribute($context["enquestum"], "id", array()))), "html", null, true);
            echo "\">Editar</a>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['enquestum'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "        </tbody>
    </table>

    ";
        // line 85
        echo "\t</div>
<script src=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/adminback.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "enquesta/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  222 => 86,  219 => 85,  214 => 77,  204 => 73,  200 => 71,  196 => 69,  192 => 67,  190 => 66,  182 => 61,  176 => 58,  170 => 55,  167 => 54,  161 => 53,  155 => 51,  152 => 50,  149 => 49,  146 => 48,  143 => 47,  140 => 46,  135 => 45,  133 => 44,  127 => 41,  124 => 40,  120 => 38,  116 => 36,  114 => 35,  107 => 33,  101 => 32,  95 => 31,  91 => 30,  88 => 29,  84 => 28,  62 => 9,  58 => 8,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
\t<nav class=\"navbar navbar-light bg-light\">
    <span id=\"logo\" class=\"btn-answer navbar-brand\">
        <img src=\"{{ asset('img/logo.png') }}\" height=\"55\" alt=\"\">
    </span>
        <span class=\"functionsplus\"><a href=\"{{ path('enquesta_new') }}\"><i class=\"fa fa-plus\"></i></a></span>
    <p class=\"text-right\">Bienvenido de nuevo <span id=\"nameadmin\"></span> !<br><a id=\"backtodash\" href=\"{{ path('homepage') }}\" class=\"btn-answer\">Dashboard <i class=\"fa fa-user-circle\"></i></a></p>
\t</nav>
\t<div class=\"container\">    
\t<h1>Llista d'enquestes</h1>
    <table class=\"tabladmin\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Pregunta</th>
                <th>Data inicial</th>
                <th>Data final</th>
                <th>Destacada</th>
                <th>Autor</th>                
                <th>Respostes</th>
                <th>Activa</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for enquestum in enquestas | reverse %}
            <tr>
                <td>{{ enquestum.id }}</td>
                <td><a href=\"{{ path('enquesta_show', { 'id': enquestum.id }) }}\">{{ enquestum.pregunta }}</a></td>
                <td>{% if enquestum.dataInici %}{{ enquestum.dataInici|date('Y-m-d') }}{% endif %}</td>
                <td>{% if enquestum.dataFinal %}{{ enquestum.dataFinal|date('Y-m-d') }}{% endif %}</td>
                <td>
                    {% if enquestum.destacada == 1 %}
                        Si
                    {% else %}
                        No
                    {% endif %}
                </td>
                <td>{{ enquestum.autor }}</td>                
                <td>
                    <ul>
                        {% set si, no = 0, 0 %}
                        {% for resposta in respostes %}
                            {% if resposta.idEnquesta == enquestum.id %}
                                {% if resposta.valor == 1 %}
                                    {% set si = si+1 %}
                                {% else %}
                                    {% set no = no+1 %}
                                {% endif %}  
                            {% endif %}
                        {% endfor %}
                        <li> 
                            Si: {{ si }}                            
                        </li>
                        <li>
                            No: {{ no }}
                        </li>
                        <li>
                            Total: {{ si + no }}
                        </li>
                    </ul>
                </td>
                <td>
                    {% if date(enquestum.dataFinal) > date(\"now\"|date(\"Y/m/d\")) %}
                        Si
                    {% else %}
                        No
                    {% endif %}
                </td>
                <td>
                    <a href=\"{{ path('enquesta_edit', { 'id': enquestum.id }) }}\">Editar</a>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    {#<ul>
        <li>
            <a href=\"{{ path('enquesta_new') }}\">Crear una nueva enquesta</a>
        </li>
    </ul>#}
\t</div>
<script src=\"{{ asset('js/adminback.js') }}\"></script>
{% endblock %}
", "enquesta/index.html.twig", "/var/www/html/thepoll/app/Resources/views/enquesta/index.html.twig");
    }
}
