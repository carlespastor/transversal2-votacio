<?php

/* @App/templates/settings.html.twig */
class __TwigTemplate_d47438acff36c73173f13fe2cafb7dab1d2e3887113563cf1fea090874d28501 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@App/base.html.twig", "@App/templates/settings.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@App/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/templates/settings.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/templates/settings.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <nav class=\"navbar navbar-light bg-light\">
        <span id=\"logo\" class=\"btn-answer navbar-brand\">
            <img src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/logo.png"), "html", null, true);
        echo "\" height=\"55\" alt=\"\">
        </span>
\t<p class=\"text-right\">Bienvenido de nuevo <span id=\"nameadmin\"></span> !<br><a id=\"backtodash\" href=\"";
        // line 7
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\" class=\"btn-answer\">Dashboard <i class=\"fa fa-user-circle\"></i></a></p>
    </nav>
    <div class=\"container soon\">
        <h2>Aun estamos trabajando en ello.</h2>
        <div class=\"fa-stack fa-4x hourglass-spin\">
\t\t  <i class=\"fa fa-stack-1x fa-hourglass-start\"></i>
\t\t  <i class=\"fa fa-stack-1x fa-hourglass-half\"></i>
\t\t  <i class=\"fa fa-stack-1x fa-hourglass-end\"></i>
\t\t  <i class=\"fa fa-stack-1x fa-hourglass-end spin\"></i>
\t\t</div>
    </div>
\t
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@App/templates/settings.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 7,  53 => 5,  49 => 3,  40 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@App/base.html.twig' %}
{% block body %}
    <nav class=\"navbar navbar-light bg-light\">
        <span id=\"logo\" class=\"btn-answer navbar-brand\">
            <img src=\"{{ asset('img/logo.png') }}\" height=\"55\" alt=\"\">
        </span>
\t<p class=\"text-right\">Bienvenido de nuevo <span id=\"nameadmin\"></span> !<br><a id=\"backtodash\" href=\"{{ path('homepage') }}\" class=\"btn-answer\">Dashboard <i class=\"fa fa-user-circle\"></i></a></p>
    </nav>
    <div class=\"container soon\">
        <h2>Aun estamos trabajando en ello.</h2>
        <div class=\"fa-stack fa-4x hourglass-spin\">
\t\t  <i class=\"fa fa-stack-1x fa-hourglass-start\"></i>
\t\t  <i class=\"fa fa-stack-1x fa-hourglass-half\"></i>
\t\t  <i class=\"fa fa-stack-1x fa-hourglass-end\"></i>
\t\t  <i class=\"fa fa-stack-1x fa-hourglass-end spin\"></i>
\t\t</div>
    </div>
\t
{% endblock %}
", "@App/templates/settings.html.twig", "/var/www/html/thepoll/src/AppBundle/Resources/views/templates/settings.html.twig");
    }
}
