<?php

/* @App/user/userDesktop.html.twig */
class __TwigTemplate_1239183496f9aa8d6d5355231553be82b1173fdddfc8bd0b056aa6e857803e28 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/user/userDesktop.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/user/userDesktop.html.twig"));

        // line 1
        echo "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.bundle.js\"></script>
<nav class=\"navbar navbar-light bg-light\">
    <span id=\"logo\" class=\"btn-answer navbar-brand\">
        <img src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/logo.png"), "html", null, true);
        echo "\" height=\"55\" alt=\"\">
    </span>
    <p class=\"text-right\">Bienvenido de nuevo <span id=\"nameadmin\"></span> !<br><a href=\"";
        // line 6
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\"><span id=\"logout\" class=\"btn-answer\">Logout <i class=\"fa fa-user-times\"></i></span></a></p>
</nav>

<div class=\"container contentajax userpolllist\"></div>

<div class=\"container cadpoll\">
    <hr>
    <p>Finalitzades:</p>

</div>

<script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/encuestasUsuario.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/hascontestado.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@App/user/userDesktop.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 18,  49 => 17,  35 => 6,  30 => 4,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<script src=\"https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.bundle.js\"></script>
<nav class=\"navbar navbar-light bg-light\">
    <span id=\"logo\" class=\"btn-answer navbar-brand\">
        <img src=\"{{ asset('img/logo.png') }}\" height=\"55\" alt=\"\">
    </span>
    <p class=\"text-right\">Bienvenido de nuevo <span id=\"nameadmin\"></span> !<br><a href=\"{{ path('homepage') }}\"><span id=\"logout\" class=\"btn-answer\">Logout <i class=\"fa fa-user-times\"></i></span></a></p>
</nav>

<div class=\"container contentajax userpolllist\"></div>

<div class=\"container cadpoll\">
    <hr>
    <p>Finalitzades:</p>

</div>

<script src=\"{{ asset('js/encuestasUsuario.js') }}\"></script>
<script src=\"{{ asset('js/hascontestado.js') }}\"></script>
", "@App/user/userDesktop.html.twig", "/var/www/html/thepoll/src/AppBundle/Resources/views/user/userDesktop.html.twig");
    }
}
