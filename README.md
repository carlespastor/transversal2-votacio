# Transversal2 --> VOTACIO

## Integrants del grup
- David Moreno
- Gerson
- Carles Pastor

## Objectiu del projecte
Es tracta d'un sistema de votacio que permet crear enquestes. Es realitzara amb la filosofia one-page

##  Estat del projecte
- Dia 1 : Definicio del projecte

[Control de tasques](https://docs.google.com/spreadsheets/d/123D_NJd32RirxiF90WoqBdf3t9D0rsUqS11qxrBHoF4/edit?usp=sharing)

[PHP Doc](https://docs.google.com/spreadsheets/d/123D_NJd32RirxiF90WoqBdf3t9D0rsUqS11qxrBHoF4/edit?usp=sharing)

[Projecte al labs](http://labs.iam.cat/~a14gerveiarc/works/transversal2/web/)

[Mockup](https://app.moqups.com/a14gerveiarc@iam.cat/Go4fw3ocjD/edit/page/aa9df7b72#)

[Diagrama casos d'us](https://drive.google.com/file/d/14nPvO_cx5WkOaRNsBDFODpbu8CLUTJFz/view?usp=sharing)
